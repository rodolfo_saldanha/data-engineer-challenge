# Data Engineering challenge

The repo implements the problem presented on `challenge/README.md` and `challenge/Simple_Architecture.pdf`.

- `challenge/` contains the files regarding the challenge description.
- `server/` implements the server. The server stubs were generated using Swagger CodeGen based on the file `challenge/swagger.yml`.
- `ansible/` provisions the EC2 instance.
- `terraform/` stands up a VPC, HTTP Server, S3 bucket and Kinesis Firehose.
- `nginx/` contains the config file of the server's Nginx service. 
- `gunicorn/` contains the config file of the server's Gunicorn service. 

## How to use

To stand up your EC2 instance, you need to set your aws credential in your terminal and change the path to your public key in `terraform/prod.tfvars`. After this, you enter `terraform/` and run:

```bash 
source deploy.sh
```

Then, you need to change the path to your instance's private key in `ansible/hosts`. After this, you can make an initial set up of your instance by entering the ansible directory and running the `initial-setup.yml` playbook.

```bash
cd ~/ansible
ansible-playbook -i hosts initial-setup.yml
```

Finally, run the following playbook to deploy the server.

```bash
ansible-playbook -i hosts deploy-server.yml
```
Inside your EC2 instance, you have to add your aws credentials. First, you have to enter your instance via SSH and create the `~/.aws/credentials`with your own credentials.

```bash
ssh ubuntu@<INSTANCE PUBLIC IP ADDRESS>
vim ~/.aws/credentials
```
Add your AWS Access Key ID and AWS Secret Access Key.

Now you are able to configure and run your server. Enter your server directory, install the server requirements and then run it.

```bash
sudo python3 setup.py install
python3 -m tracking_server
```
At this point, your server should be running and you should be able to do POST requests to it as described in the `swagger.yml`.

## Load testing (Locust)

You will find `locustfile.py` inside `server/`. To load test your server, you have enter the server directory and run:

```bash
locust -f locustfile.py --host=http://<INSTANCE PUBLIC IP ADDRESS>
```

You access http://localhost:8089 and you will be able to manage the amount of users and the spawn rate.