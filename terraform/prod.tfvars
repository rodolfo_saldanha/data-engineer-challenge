app_name       = "de-challenge"
stage          = "dev"
ssh_key_name   = "~/.ssh/id_rsa.pub"

server_instance_type                 = "t2.micro"
http_server_elastic_ip_allocation_id = "eipalloc-02b6a562c1fa80fe2"

rds_instance_type = "db.t2.micro"
database_port     = 3306 
database_user     = "rodolfo"
database_password = "sTrOnG74#pAsSwOrD"

s3_bucket_name = "firehose-bucket-exce"