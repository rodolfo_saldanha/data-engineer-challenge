output "vpc_arn" {
  value = aws_vpc.main.arn
}

output "vpc_id" {
  value = aws_vpc.main.id
}

output "server_security_group_ids" {
  value = [aws_security_group.sg_web.id]
}

output "subnet1" {
  value = aws_subnet.subnet1.id
}

output "subnet2" {
  value = aws_subnet.subnet2.id
}

output "public_gateway" {
  value = aws_internet_gateway.gw
}
