data "aws_region" "default" {}

resource "aws_kinesis_stream" "kinesis_stream" {
  name             = "${var.app_name}-${var.stage}-kinesis-stream"
  shard_count      = 1

  tags = {
    Service     = var.app_name
    Environment = var.stage
  }
}

resource "aws_kinesis_firehose_delivery_stream" "kinesis_firehose_stream" {
  name        = "${var.app_name}-${var.stage}-kinesis-firehose-stream"
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn       = aws_iam_role.kinesis_firehose_stream_role.arn
    bucket_arn     = var.aws_s3_bucket.arn
    prefix         = "logs/"
  }

  tags = {
    Service     = var.app_name
    Environment = var.stage
  }
}

data "aws_iam_policy_document" "kinesis_firehose_stream_assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["firehose.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "kinesis_firehose_access_bucket_assume_policy" {
  statement {
    effect = "Allow"

    actions = [
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:PutObject",
    ]

    resources = [
      var.aws_s3_bucket.arn,
      "${var.aws_s3_bucket.arn}/*",
    ]
  }
}

resource "aws_iam_role" "kinesis_firehose_stream_role" {
  name               = "${var.app_name}-${var.stage}-kinesis-firehose-stream-role"
  assume_role_policy = data.aws_iam_policy_document.kinesis_firehose_stream_assume_role.json

  tags = {
    Service     = var.app_name
    Environment = var.stage
  }
}

resource "aws_iam_role_policy" "kinesis_firehose_access_bucket_policy" {
  name   = "${var.app_name}-${var.stage}-kinesis-firehose-access-bucket-policy"
  role   = aws_iam_role.kinesis_firehose_stream_role.name
  policy = data.aws_iam_policy_document.kinesis_firehose_access_bucket_assume_policy.json
}
