variable "app_name" {
  description = "The name of the App that will be launch"
}

variable "stage" {
  description = "Environment to launch"
}

variable "aws_s3_bucket" {
  description = "S3 bucket"
}
