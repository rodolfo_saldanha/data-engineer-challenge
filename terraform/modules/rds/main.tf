resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "${var.app_name}-${var.stage}-rds-subnet-group"
  subnet_ids = [var.subnet1, var.subnet2]

  tags = {
    Service     = var.app_name
    Environment = var.stage
  }
}

resource "aws_security_group" "sg_db" {
  name       = "${var.app_name}-${var.stage}-sg-db"
  vpc_id = var.vpc_id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [var.subnet1]
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Service     = var.app_name
    Environment = var.stage
  }
}

resource "aws_db_instance" "mysql_db" {
  name                     = "${var.app_name}-${var.stage}-mysqlrds"
  identifier               = "mysqlrds"
  allocated_storage        = 20 # gigabytes
  backup_retention_period  = 7
  engine                   = "mysql"
  engine_version           = "5.7"
  instance_class           = var.rds_instance_type
  username                 = var.database_user
  password                 = var.database_password
  port                     = var.database_port
  storage_type             = "gp2"
  db_subnet_group_name     = aws_db_subnet_group.rds_subnet_group.name
  vpc_security_group_ids   = [aws_security_group.sg_db.id]
  skip_final_snapshot      = true

  tags = {
    Service     = var.app_name
    Environment = var.stage
  }
} 