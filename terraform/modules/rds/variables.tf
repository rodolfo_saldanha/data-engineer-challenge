variable "app_name" {
  description = "The name of the App that will be launch"
}

variable "vpc_id" {
  description = "VPC id"
}

variable "subnet1" {
  description = "Subnet"
}

variable "subnet2" {
  description = "Subnet"
}

variable "rds_instance_type" {
  description = "RDS instance type"
}

variable "database_user" {
  description = "Database user"
}

variable "database_password" {
  description = "Database password"
}

variable "database_port" {
  description = "Database port"
}