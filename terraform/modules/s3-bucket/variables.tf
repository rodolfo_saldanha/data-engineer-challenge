variable "app_name" {
  description = "The name of the App that will be launch"
}

variable "stage" {
  description = "Environment to launch"
}

variable "s3_bucket_name" {
  description = "S3 bucket name"
}