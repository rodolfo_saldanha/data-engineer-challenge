resource "aws_s3_bucket" "kinesis_firehose_bucket" {
  bucket = "${var.app_name}-${var.stage}-${var.s3_bucket_name}"
  acl    = "private"

  tags = {
    Service  = var.app_name
    Environment = var.stage
  }
}