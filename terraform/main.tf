provider "aws" {
    version = "~> 2.70"
    region  = "us-east-2" 
}

terraform {
  required_version = "~>0.12"
  backend "s3" {
    region = "us-east-2" 
    bucket = "rodolfo-vpc-bucket"
    key    = "tfstates/"
  }
}

module "vpc" {
  source         = "./modules/vpc"
  app_name       = var.app_name
  stage          = var.stage
  database_port  = var.database_port
}

module "http-server" {
  source                   = "./modules/http-server"
  app_name                 = var.app_name
  stage                    = var.stage
  ssh_key_name             = var.ssh_key_name
  subnet_id                = module.vpc.subnet1
  vpc_security_group_ids   = module.vpc.server_security_group_ids
  instance_type            = var.server_instance_type
  elastic_ip_allocation_id = var.http_server_elastic_ip_allocation_id
}

module "s3-bucket" {
  app_name       = var.app_name
  stage          = var.stage
  source         = "./modules/s3-bucket"
  s3_bucket_name = var.s3_bucket_name
}

module "firehose" {
  app_name      = var.app_name
  stage         = var.stage
  source        = "./modules/firehose"
  aws_s3_bucket = module.s3-bucket.aws_s3_bucket
}