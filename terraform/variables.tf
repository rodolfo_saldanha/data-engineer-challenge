variable "app_name" {
  description = "The name of the application"
}

variable "stage" {
  description = "Environment to launch"
}

variable "server_instance_type" {
  description = "Server instance type"
}

variable "rds_instance_type" {
  description = "RDS instance type"
}

variable "http_server_elastic_ip_allocation_id" {
  description = "Server Elastic IP"
}

variable "ssh_key_name" {
  description = "SSH key name"
}

variable "database_user" {
  description = "Database user"
}

variable "database_password" {
  description = "Database password"
}

variable "database_port" {
  type = number
  description = "Port that the database uses"
}

variable "s3_bucket_name" {
  description = "S3 bucket name"
}