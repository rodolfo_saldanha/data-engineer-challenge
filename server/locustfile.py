from locust import HttpUser, TaskSet, task


headers = {'Content-type': 'application/json', 'Accept': 'application/json'}


class UserBehavior(TaskSet):
    def on_start(self):
        """ on_start is called when a Locust start before
            any task is scheduled
        """
        self.login()


class WebsiteUser(HttpUser):
    @task()
    def track(self):
        self.client.post("/track",
                         json={"userId": "string",
                               "events": [
                                   {"eventName": "string",
                                    "metadata": {},
                                    "timestampUTC": 0
                                    }
                                ]
                               },
                         headers=headers)

    @task()
    def alias(self):
        self.client.post("/alias",
                         json={"newUserId": "string",
                               "originalUserId": "string",
                               "timestampUTC": 0
                               },
                         headers=headers)

    @task()
    def profile(self):
        self.client.post("/profile",
                         json={"userId": "string",
                               "attributes": {},
                               "timestampUTC": 0
                               },
                         headers=headers)
